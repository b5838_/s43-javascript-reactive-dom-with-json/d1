// console.log("Hello World")
// Scenario: Let's create a simple program that will perform CRUD operation for posting the available movies to watch.

// Mock Database
let posts = []
// Posts ID
let count = 1;

// Add post data
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
                    // selects an id form
document.querySelector("#form-add-post")
.addEventListener("submit", (e) => {

    // Prevents the page from loading
    e.preventDefault();
    posts.push({
        // "id" property will be used for the unique identification of each posts.
        id: count,
        // "title" & "body" values will come from the "form-add-post" input elements.
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })
    // count will increament every time a new movie is added.
    count++;

    console.log(posts);
    alert("Successfully added!");
    showPosts();
})

// View Posts
const showPosts = () => {
    let postEntries = "";

    // We will used forEach() to display each movie inside our mock database.
    posts.forEach(post => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });

    // To check what is stored in the postEntries variables
    console.log(postEntries)

    // To replace the content of the "div-post-entries"
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Edit Post Button
// We will create a function that will be called in the onclick() event and will pass the value in the Update Form input box.
const editPost = (id) => {
    // Contain the value of the title and body in a variable.
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    // Pass the id, title, and body of the moview post to be updated in the Edit Post/Form.
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

};

// Update post
// This will trigger an event that will update a post upon clicking the Update button.

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    let postId = document.querySelector("#txt-edit-id").value;
    let title = document.querySelector("#txt-edit-title").value;
    let body = document.querySelector("#txt-edit-body").value;
    
    // Use a loop that will check each post stored in our mock database.
    for(i = 0; i < posts.length; i++){
        // Use a if statement to look for the post to be updated using id property.
        if(posts[i].id == postId){
            // reassign the value of the title and body property
            posts[i].title = title;
            posts[i].body = body;

            // Invoke the showPosts() to check the updated posts.
            showPosts();

            alert("Successfully Updated!");
            break;
        }
    };
});

// Delete post
/*
s43 Activity:
    -Using the code so far, write the necessary code to delete a post.
    -Create the function (deletePost(id)) invoked on the “onclick” event of the delete button.
    -Make sure that the Now Showing List will be updated upon clicking the delete button.
    -Once done create a remote link and push to git with the commit message of WDC028-43.
    -Add the link in Boodle.
*/
const deletePost = (id) => {
    let postId = posts.indexOf(id);
    posts.splice(postId, 1);
    showPosts();
}

/*------ Solution By Sir Angelito Quiambao -----*/
// const deletePost = (id) => {
//     // filter method will save the posts that are not equal to the id parameter.
//     posts = posts.filter((post) => {
//         if(post.id != id){
//             return post;
//         } 
//     })

//     console.log(posts);
//     // .remove() method removes an elemet (or node) from the document.
//     document.querySelector(`#post-${id}`).remove();
// }